package practica1;

import java.util.ArrayList;

public class Solucion {
    int fichasRestantes;
    ArrayList<Movimiento> movimientos = new ArrayList<>();
    int puntuacionFinal;

    public Solucion() {
    }

    public Solucion(int fichasRestantes, ArrayList<Movimiento> movimientos) {
        this.fichasRestantes = fichasRestantes;
        this.movimientos = movimientos;
    }

    public Solucion(int puntuacionFinal, int fichasRestantes, ArrayList<Movimiento> movimientos) {
        this.fichasRestantes = fichasRestantes;
        this.movimientos = movimientos;
        this.puntuacionFinal = puntuacionFinal;
    }

    public int getFichasRestantes() {
        return fichasRestantes;
    }

    public void setFichasRestantes(int fichasRestantes) {
        this.fichasRestantes = fichasRestantes;
    }

    public ArrayList<Movimiento> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(ArrayList<Movimiento> movimientos) {
        this.movimientos = movimientos;
    }

    public int getPuntuacionFinal() {
        int puntuacionFinal = 0;
        for (Movimiento movimiento : this.movimientos) {
            puntuacionFinal += movimiento.getPuntos();
        }

        if (fichasRestantes == 0) {
            puntuacionFinal += 1000;
        }
        return puntuacionFinal;
    }

    public void setPuntuacionFinal(int puntuacionFinal) {
        this.puntuacionFinal = puntuacionFinal;
    }

    public String toString() {
        StringBuffer salida = new StringBuffer();

        for (Movimiento movimiento : movimientos) {
            salida.append(movimiento.toString());
        }

        salida.append("Puntuación final: " + this.getPuntuacionFinal() + ", quedando " + fichasRestantes + " "
                + Main.pluralizarString("ficha", fichasRestantes) + ".\n");

        return salida.toString();
    }
}