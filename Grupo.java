package practica1;

import java.util.*;

public class Grupo {
    char letra;
    ArrayList<int[]> posiciones = new ArrayList<>();
    int tamanioGrupo;

    public Grupo() {
    }

    public Grupo(char letra, ArrayList<int[]> posiciones) {
        this.letra = letra;
        this.posiciones = posiciones;
        this.tamanioGrupo = posiciones.size();
    }

    public int getTamanioGrupo() {
        return tamanioGrupo;
    }

    public void setTamanioGrupo(int tamanioGrupo) {
        this.tamanioGrupo = posiciones.size();
    }

    public char getLetra() {
        return letra;
    }

    public void setLetra(char letra) {
        this.letra = letra;
    }

    public ArrayList<int[]> getPosiciones() {
        return posiciones;
    }

    public void setPosiciones(ArrayList<int[]> posiciones) {
        this.posiciones = posiciones;
    }

    public String posicionesToString() {
        StringBuilder sb = new StringBuilder();
        for (int[] posicion : posiciones) {
            sb.append("[" + posicion[0] + "," + posicion[1] + "]");
            sb.append(" , ");
        }
        return sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Letra: ").append(letra).append("\n");
        sb.append("Posiciones: ").append(posicionesToString()).append("\n");
        sb.append("Tamaño del grupo: ").append(tamanioGrupo);
        return sb.toString();
    }
}