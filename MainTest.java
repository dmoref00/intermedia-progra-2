package practica1;

import static org.junit.Assert.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MainTest {

    private static InputStream in;
    private static PrintStream out;

    /**
     * Este método se ejecuta antes de la ejecución de cada test.
     * Permite almacenar los flujos que tiene la máquina java
     * para la entrada y salida estándar
     * 
     * @throws Exception
     */

    @Before
    public void realizaAntesDeCadaTest() throws Exception {
        in = System.in;
        out = System.out;
    }

    /**
     * Este método se ejecuta después de cada test. Permite restaurar
     * los flujos de entrada y salida estándar
     * 
     * @throws Exception
     */
    @After
    public void realizaDespuésDeCadaTest() throws Exception {
        System.setIn(in);
        System.setOut(out);
    }

    //test de adolfo
    @Test
    public void testPrincipal() throws Exception {
        String entradaTest = "2\r\n" + //
                            "\r\n" + //
                            "AVR\r\n" + //
                            "AAR\r\n" + //
                            "ARR\r\n" + //
                            "VVR\r\n" + //
                            "\r\n" + //
                            "VRA\r\n" + //
                            "RAR\r\n" + //
                            "AAR\r\n" + //
                            "RVV";


        String salidaEsperadaTest = "Juego 1:\r\n" + //
                "Movimiento 1 en (1, 3): eliminó 5 fichas de color R y obtuvo 9 puntos.\n" + //
                "Movimiento 2 en (2, 1): eliminó 4 fichas de color A y obtuvo 4 puntos.\n" + //
                "Movimiento 3 en (1, 1): eliminó 3 fichas de color V y obtuvo 1 punto.\n" + //
                "Puntuación final: 1014, quedando 0 fichas.\n" + //
                "\r\n" + //
                "Juego 2:\r\n" + //
                "Movimiento 1 en (2, 3): eliminó 2 fichas de color R y obtuvo 0 puntos.\n" + //
                "Movimiento 2 en (2, 1): eliminó 4 fichas de color A y obtuvo 4 puntos.\n" + //
                "Movimiento 3 en (1, 1): eliminó 3 fichas de color R y obtuvo 1 punto.\n" + //
                "Movimiento 4 en (1, 1): eliminó 3 fichas de color V y obtuvo 1 punto.\n" + //
                "Puntuación final: 1006, quedando 0 fichas.\n";

        // Redirigimos la entrada estándar
        InputStream nuevo_in = new ByteArrayInputStream(entradaTest.getBytes());
        System.setIn(nuevo_in);

        // Redirigimos la salida estándar
        ByteArrayOutputStream salidaRealTest = new ByteArrayOutputStream();
        PrintStream nuevo_out = new PrintStream(salidaRealTest);
        System.setOut(nuevo_out);

        // Llamo a main con su argumento igual a un array de cadenas vacío
        // Gracias a la redirección de la entrada/salida estándar
        // main leerá de la cadena entradaTest y escribirá al
        // array de bytes salidaRealTest
        Main.main(new String[0]);

        // El test tendrá éxito si la salida real es igual a la esperada
        assertEquals(salidaEsperadaTest, salidaRealTest.toString());
    }

    @Test
    public void test1MatrizErroneaLetra() throws Exception {
        String entradaTest = "1\r\n" + //
                            "\r\n" + //
                            "AAA\r\n" + //
                            "EEE ";
        String salidaEsperadaTest = "";

        // Redirigimos la entrada estándar
        InputStream nuevo_in = new ByteArrayInputStream(entradaTest.getBytes());
        System.setIn(nuevo_in);

        // Redirigimos la salida estándar
        ByteArrayOutputStream salidaRealTest = new ByteArrayOutputStream();
        PrintStream nuevo_out = new PrintStream(salidaRealTest);
        System.setOut(nuevo_out);

        Main.main(new String[0]);

        // El test tendrá éxito si la salida real es igual a la esperada
        assertEquals(salidaEsperadaTest, salidaRealTest.toString());
    }

    //Test con 0 filas
    @Test
    public void test1MatrizErroneaFilasMin() throws Exception {
        String entradaTest = "1\r\n" + //
                            "\r\n" + //
                            "";
        String salidaEsperadaTest = "";

        // Redirigimos la entrada estándar
        InputStream nuevo_in = new ByteArrayInputStream(entradaTest.getBytes());
        System.setIn(nuevo_in);

        // Redirigimos la salida estándar
        ByteArrayOutputStream salidaRealTest = new ByteArrayOutputStream();
        PrintStream nuevo_out = new PrintStream(salidaRealTest);
        System.setOut(nuevo_out);

        Main.main(new String[0]);

        // El test tendrá éxito si la salida real es igual a la esperada
        assertEquals(salidaEsperadaTest, salidaRealTest.toString());
    }

    //Test con 21 columnas
    @Test
    public void test1MatrizErroneaColumnasMax() throws Exception {
        String entradaTest = "1\r\n" + //
                            "\r\n" + //
                            "AAAAAAAAAAAAAAAAAAAAA";
        String salidaEsperadaTest = "";

        // Redirigimos la entrada estándar
        InputStream nuevo_in = new ByteArrayInputStream(entradaTest.getBytes());
        System.setIn(nuevo_in);

        // Redirigimos la salida estándar
        ByteArrayOutputStream salidaRealTest = new ByteArrayOutputStream();
        PrintStream nuevo_out = new PrintStream(salidaRealTest);
        System.setOut(nuevo_out);

        Main.main(new String[0]);

        // El test tendrá éxito si la salida real es igual a la esperada
        assertEquals(salidaEsperadaTest, salidaRealTest.toString());
    }

    //Test con 20 columnas
    @Test
    public void test1MatrizErroneaColumnasLimiteMax() throws Exception {
        String entradaTest = "1\r\n" + //
                            "\r\n" + //
                            "AAAAAAAAAAAAAAAAAAAA";
        String salidaEsperadaTest = "Juego 1:\r\n" + //
                "Movimiento 1 en (1, 1): eliminó 20 fichas de color A y obtuvo 324 puntos.\n" + //
                "Puntuación final: 1324, quedando 0 fichas.\n";

        // Redirigimos la entrada estándar
        InputStream nuevo_in = new ByteArrayInputStream(entradaTest.getBytes());
        System.setIn(nuevo_in);

        // Redirigimos la salida estándar
        ByteArrayOutputStream salidaRealTest = new ByteArrayOutputStream();
        PrintStream nuevo_out = new PrintStream(salidaRealTest);
        System.setOut(nuevo_out);

        Main.main(new String[0]);

        // El test tendrá éxito si la salida real es igual a la esperada
        assertEquals(salidaEsperadaTest, salidaRealTest.toString());
    }

    //Test con 0 matrices
    @Test
    public void test0Matrices() throws Exception {
        String entradaTest = "0";
        String salidaEsperadaTest = "";

        // Redirigimos la entrada estándar
        InputStream nuevo_in = new ByteArrayInputStream(entradaTest.getBytes());
        System.setIn(nuevo_in);

        // Redirigimos la salida estándar
        ByteArrayOutputStream salidaRealTest = new ByteArrayOutputStream();
        PrintStream nuevo_out = new PrintStream(salidaRealTest);
        System.setOut(nuevo_out);

        Main.main(new String[0]);

        // El test tendrá éxito si la salida real es igual a la esperada
        assertEquals(salidaEsperadaTest, salidaRealTest.toString());
    }

    //Test con la primera matriz bien la segunda mal
    // solo imprime la primera
    @Test
    public void test1MatrizBien1Mal() throws Exception {
        String entradaTest = "2\r\n" + //
                "\r\n" + //
                "AVR\r\n" + //
                "AAR\r\n" + //
                "ARR\r\n" + //
                "VVR\r\n" + //
                "\r\n" + //
                "VRA\r\n" + //
                "RAR\r\n" + //
                "AAE\r\n" + //
                "RVV";
        String salidaEsperadaTest = "Juego 1:\r\n" + //
                "Movimiento 1 en (1, 3): eliminó 5 fichas de color R y obtuvo 9 puntos.\n" + //
                "Movimiento 2 en (2, 1): eliminó 4 fichas de color A y obtuvo 4 puntos.\n" + //
                "Movimiento 3 en (1, 1): eliminó 3 fichas de color V y obtuvo 1 punto.\n" + //
                "Puntuación final: 1014, quedando 0 fichas.\n";

        // Redirigimos la entrada estándar
        InputStream nuevo_in = new ByteArrayInputStream(entradaTest.getBytes());
        System.setIn(nuevo_in);

        // Redirigimos la salida estándar
        ByteArrayOutputStream salidaRealTest = new ByteArrayOutputStream();
        PrintStream nuevo_out = new PrintStream(salidaRealTest);
        System.setOut(nuevo_out);

        Main.main(new String[0]);

        // El test tendrá éxito si la salida real es igual a la esperada
        assertEquals(salidaEsperadaTest, salidaRealTest.toString());
    }

    //Test con la primera matriz bien la segunda mal y la tercera bien
    // solo imprime la primera
    @Test
    public void test1MatrizBien1Mal1Bien() throws Exception {
        String entradaTest = "2\r\n" + //
                "\r\n" + //
                "AVR\r\n" + //
                "AAR\r\n" + //
                "ARR\r\n" + //
                "VVR\r\n" + //
                "\r\n" + //
                "VRA\r\n" + //
                "RAR\r\n" + //
                "AAE\r\n" + //
                "RVV" + //
                "\r\n" + //
                "VRA\r\n" + //
                "RAR\r\n" + //
                "AAE\r\n" + //
                "RVV";
        String salidaEsperadaTest = "Juego 1:\r\n" + //
                "Movimiento 1 en (1, 3): eliminó 5 fichas de color R y obtuvo 9 puntos.\n" + //
                "Movimiento 2 en (2, 1): eliminó 4 fichas de color A y obtuvo 4 puntos.\n" + //
                "Movimiento 3 en (1, 1): eliminó 3 fichas de color V y obtuvo 1 punto.\n" + //
                "Puntuación final: 1014, quedando 0 fichas.\n";

        // Redirigimos la entrada estándar
        InputStream nuevo_in = new ByteArrayInputStream(entradaTest.getBytes());
        System.setIn(nuevo_in);

        // Redirigimos la salida estándar
        ByteArrayOutputStream salidaRealTest = new ByteArrayOutputStream();
        PrintStream nuevo_out = new PrintStream(salidaRealTest);
        System.setOut(nuevo_out);

        Main.main(new String[0]);

        // El test tendrá éxito si la salida real es igual a la esperada
        assertEquals(salidaEsperadaTest, salidaRealTest.toString());
    }


    //test de la matriz del enunciado
    @Test
    public void testEnunciado() throws Exception {
        String entradaTest = "1\r\n" + //
                            "\r\n" + //
                            "RRRVAA\r\n" + //
                            "RAVVVA\r\n" + //
                            "VVVVAA";


        String salidaEsperadaTest = "Juego 1:\r\n" + //
                                    "Movimiento 1 en (1, 5): eliminó 5 fichas de color A y obtuvo 9 puntos.\n" + //
                                    "Movimiento 2 en (2, 1): eliminó 4 fichas de color R y obtuvo 4 puntos.\n" + //
                                    "Movimiento 3 en (1, 1): eliminó 8 fichas de color V y obtuvo 36 puntos.\n" + //
                                    "Puntuación final: 49, quedando 1 ficha.\n";

        // Redirigimos la entrada estándar
        InputStream nuevo_in = new ByteArrayInputStream(entradaTest.getBytes());
        System.setIn(nuevo_in);

        // Redirigimos la salida estándar
        ByteArrayOutputStream salidaRealTest = new ByteArrayOutputStream();
        PrintStream nuevo_out = new PrintStream(salidaRealTest);
        System.setOut(nuevo_out);

        // Llamo a main con su argumento igual a un array de cadenas vacío
        // Gracias a la redirección de la entrada/salida estándar
        // main leerá de la cadena entradaTest y escribirá al
        // array de bytes salidaRealTest
        Main.main(new String[0]);

        // El test tendrá éxito si la salida real es igual a la esperada
        assertEquals(salidaEsperadaTest, salidaRealTest.toString());
    }

    //test de adolfo
    @Test
    public void testRecrearMatriz() throws Exception {
        char[][] matriz = {{'R','R','R','V','A','A'},
                            {' ',' ',' ','V','V','A'},
                            {'V',' ','V',' ','A','A'}};

        char[][] matrizEsperada = {{' ',' ',' ',' ','A','A'},
                                    {'R',' ','R','V','V','A'},
                                    {'V','R','V','V','A','A'}};

        Main.recrearMatriz(matriz);

        Main.imprimirMatriz(matrizEsperada);
        Main.imprimirMatriz(matriz);
    }

    
}