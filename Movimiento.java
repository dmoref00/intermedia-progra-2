package practica1;

public class Movimiento {
    int numMovimiento;
    int[] coordenada;
    Grupo grupo = new Grupo();
    int puntos;

    public Movimiento() {
    }

    public Movimiento(int movimiento, int[] coordenada, Grupo grupo, int puntos) {
        this.numMovimiento = movimiento;
        this.coordenada = coordenada;
        this.grupo = grupo;
        this.puntos = puntos;
    }

    public int[] getCoordenada() {
        return coordenada;
    }

    public void setCoordenada(int[] coordenada) {
        this.coordenada = coordenada;
    }

    public int getNumMovimiento() {
        return numMovimiento;
    }

    public void setNumMovimiento(int movimiento) {
        this.numMovimiento = movimiento;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {

        this.puntos = puntos;
    }

    @Override
    public String toString() {
        String salida;
        salida = ("Movimiento " + numMovimiento + " en (" + coordenada[0] + ", "
                + coordenada[1] + "): eliminó " + grupo.tamanioGrupo
                + " fichas de color " + grupo.letra + " y obtuvo " + puntos + " "
                + Main.pluralizarString("punto", puntos) + ".\n");

        return salida;
    }
}