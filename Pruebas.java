package practica1;

public class Pruebas {
    public static void main(String[] args) throws Exception {
        testRecrearMatriz();
    }
    public static void testRecrearMatriz() throws Exception {
        char[][] matriz = {{'R','R',' ','V','A','A'},
                            {' ',' ',' ','V','V','A'},
                            {'V',' ',' ',' ','A','A'}};

        char[][] matrizEsperada = {{' ',' ',' ','A','A'},
                                    {'R',' ','V','V','A'},
                                    {'V','R','V','A','A'}};

        Main.recrearMatriz(matriz);

        Main.imprimirMatriz(matrizEsperada);
        Main.imprimirMatriz(matriz);
    }
}
