package practica1;

import java.util.*;

public class Main {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int numMatrices = sc.nextInt();
        ArrayList<char[][]> matrices = new ArrayList<>();

        obtenerMatrices(matrices, numMatrices, sc);

        for (int i = 0; i < matrices.size(); i++) {
            System.out.println("Juego " + (i + 1) + ":");

            Solucion solucion = resolverMatriz1(matrices.get(i));

            System.out.print(solucion.toString());

            // Pone una linea vacia entre los outputs de juegos, no lo hace para el ultimo
            if (i < matrices.size() - 1) {
                System.out.println();
            }
        }
    }

    // resuelve la matriz
    public static void resolverMatriz(char[][] matriz, int numMovimiento,
            ArrayList<Solucion> soluciones, ArrayList<Movimiento> movimientos) {

        ArrayList<Grupo> listaGrupos = encontrarTodosLosGrupos(matriz);
        

        // Matriz resuelta
        if (listaGrupos.isEmpty() || todosLosGruposTienenTamanio1(listaGrupos)) {
            // meter aqui soluciones
            ArrayList<Movimiento> nuevosMovimientos = copiarMovimientos(movimientos);
            Solucion solucion = new Solucion(fichasRestantesMatriz(matriz), nuevosMovimientos);
            soluciones.add(solucion);
            return;
        }

        // Prueba todos los movimientos
        for (int i = 0; i < listaGrupos.size(); i++) {

            Grupo grupoAEliminar = listaGrupos.get(i);

            //Comprueba que el grupo elegido es mayor o igual a 2
            if (grupoAEliminar.tamanioGrupo >= 2) {

                char[][] nuevaMatriz = copiarMatriz(matriz);

                Movimiento movimiento = new Movimiento(numMovimiento + 1,
                        convertirPosicionSalida(encontrarPosicionMasAbajoIzquierda(grupoAEliminar), nuevaMatriz),
                        grupoAEliminar,
                        (int) Math.pow((eliminarGrupo(listaGrupos, grupoAEliminar, nuevaMatriz) - 2), 2));

                movimientos.add(movimiento);

                nuevaMatriz = recrearMatriz(nuevaMatriz);

                resolverMatriz(nuevaMatriz, numMovimiento + 1, soluciones, movimientos);

                movimientos.remove(movimiento);

                listaGrupos = encontrarTodosLosGrupos(matriz);
            }
        }

        return;
    }

    // aqui empieza el backtracking
    public static Solucion resolverMatriz1(char[][] matriz) {
        int numMovimiento = 0;
        ArrayList<Solucion> soluciones = new ArrayList<>();
        ArrayList<Movimiento> movimientos = new ArrayList<>();

        // backtracking

        resolverMatriz(matriz, numMovimiento, soluciones, movimientos);

        // Ver todas las soluciones
        //System.out.println("Numero soluciones " + soluciones.size());
        
         /* for (Solucion solucion : soluciones) {
         System.out.println(solucion.toString());
         } */
        
        Solucion solucionMasAlta = compararSoluciones(soluciones);

        return solucionMasAlta;
    }

    public static Solucion compararSoluciones(ArrayList<Solucion> soluciones) {
        if (soluciones.isEmpty()) {
            return null;
        }

        Solucion solucionMasAlta = soluciones.get(0);

        for (Solucion solucion : soluciones) {
            // si es mayor que la anterior
            if (solucion.getPuntuacionFinal() > solucionMasAlta.getPuntuacionFinal()) {
                solucionMasAlta = solucion;
            }
            // si es igual, orden lexicográfico
            else if (solucion.getPuntuacionFinal() == solucionMasAlta.getPuntuacionFinal()) {
                for (int i = 0; i < solucion.movimientos.size(); i++) {
                    if (solucion.movimientos.get(i).coordenada[0] < solucionMasAlta.movimientos.get(i).coordenada[0]) {
                        solucionMasAlta = solucion;
                        break;
                    } else if (solucion.movimientos.get(i).coordenada[1] < solucionMasAlta.movimientos
                            .get(i).coordenada[1]) {
                        solucionMasAlta = solucion;
                        break;
                    }
                }
            }
        }

        return solucionMasAlta;
    }

    public static void imprimirSalida(Solucion solucion) {
        System.out.println(solucion.toString());
    }

    // devuelve false si la matriz esta mal formada
    private static boolean comprobarMatriz(char[][] matriz) {
        // Comprueba que la matriz esta dentro de los tamanios especificados
        if (matriz.length < 1 || matriz[0].length > 20) {
            return false;
        }

        // comprueba que la matriz es regular, es decir que no contiene nada que no sea
        // A, V o R
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if (matriz[i][j] != 'A' && matriz[i][j] != 'R' && matriz[i][j] != 'V') {
                    return false;
                }
            }
        }
        return true;
    }

    // para cada letra y grupo independiente, agrupa las letras adyacentes
    public static ArrayList<Grupo> encontrarTodosLosGrupos(char[][] matriz) {
        ArrayList<Grupo> grupos = new ArrayList<>();

        int filas = matriz.length;
        int columnas = matriz[0].length;

        boolean[][] visitado = new boolean[filas][columnas];

        for (int fila = 0; fila < filas; fila++) {
            for (int columna = 0; columna < columnas; columna++) {
                if (!visitado[fila][columna]) {
                    char letra = matriz[fila][columna];

                    ArrayList<int[]> posiciones = new ArrayList<>();
                    dfs(matriz, fila, columna, letra, visitado, posiciones);
                    Grupo grupo = new Grupo(letra, posiciones);
                    grupos.add(grupo);
                }
            }
        }

        ArrayList<Grupo> gruposAEliminar = new ArrayList<>();

        for (Grupo grupo : grupos) {
            if (grupo.letra == ' ') {
                gruposAEliminar.add(grupo);
            }
        }

        grupos.removeAll(gruposAEliminar);

        return grupos;
    }

    public static boolean todosLosGruposTienenTamanio1(ArrayList<Grupo> listaGrupos) {
        for (Grupo grupo : listaGrupos) {
            if (grupo.tamanioGrupo > 1) {
                return false;
            }
        }

        return true;
    }

    public static int fichasRestantesMatriz(char[][] matriz) {
        int fichas = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if (!(matriz[i][j] == ' ')) {
                    fichas++;
                }
            }
        }
        return fichas;
    }

    // busca y añade a una lista todas las posiciones adyacentes con la misma letra
    private static void dfs(char[][] matriz, int fila, int columna, char letra,
            boolean[][] visitado, ArrayList<int[]> posiciones) {
        int filas = matriz.length;
        int columnas = matriz[0].length;

        if (fila < 0 || fila >= filas || columna < 0 || columna >= columnas || visitado[fila][columna]
                || matriz[fila][columna] != letra) {
            return;
        }

        visitado[fila][columna] = true;
        posiciones.add(new int[] { fila, columna });

        dfs(matriz, fila + 1, columna, letra, visitado, posiciones); // Abajo
        dfs(matriz, fila - 1, columna, letra, visitado, posiciones); // Arriba
        dfs(matriz, fila, columna + 1, letra, visitado, posiciones); // Derecha
        dfs(matriz, fila, columna - 1, letra, visitado, posiciones); // Izquierda
    }

    public static void obtenerMatrices(ArrayList<char[][]> matrices, int numMatrices, Scanner sc) {
        boolean matricesBien = true;
        eliminarLineaVacia(sc); // Eliminar la línea en blanco antes de cada matriz
        eliminarLineaVacia(sc);

        for (int i = 0; i < numMatrices; i++) {
            char[][] matriz = crearMatriz(sc);
            if (!comprobarMatriz(matriz)) {
                //Si la matriz esta mal formada, dejar de meter matrices al array
                matricesBien = false;
            }
            if (matriz != null && matricesBien) {
                matrices.add(matriz);
            }
        }
    }

    public static char[][] crearMatriz(Scanner sc) {
        ArrayList<char[]> filas = new ArrayList<>();
        String input;

        // Lee todas las lineas de la entrada estandar
        while (sc.hasNextLine()) {
            input = sc.nextLine();
            // System.out.println(input);
            if (input.isEmpty()) {
                break;
            }
            filas.add(input.toCharArray());
        }

        // Calcula cuantas columnas tiene la matriz
        int maxColumnas = 0;
        for (char[] fila : filas) {
            if (fila.length > maxColumnas) {
                maxColumnas = fila.length;
            }
        }

        // Crea la matriz
        char[][] matriz = new char[filas.size()][maxColumnas];
        for (int i = 0; i < filas.size(); i++) {
            for (int j = 0; j < filas.get(i).length; j++) {
                matriz[i][j] = filas.get(i)[j];
            }
        }

        return matriz;
    }

    public static void imprimirMatrices(ArrayList<char[][]> matrices) {
        for (int i = 0; i < matrices.size(); i++) {
            imprimirMatriz(matrices.get(i));
            System.out.println();
        }
    }

    public static void imprimirMatriz(char[][] matriz) {
        for (char[] fila : matriz) {
            for (char caracter : fila) {
                System.out.print(caracter);
            }
            System.out.println();
        }
    }

    public static void eliminarLineaVacia(Scanner sc) {
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line.isEmpty()) {
                break;
            }
        }
    }

    public static void imprimirGrupos(ArrayList<Grupo> listaGrupos) {
        for (int i = 0; i < listaGrupos.size(); i++) {
            System.out.println("Grupo: " + i);
            imprimirGrupo(listaGrupos.get(i));
        }
    }

    private static void imprimirGrupo(Grupo grupo) {
        System.out.println(grupo.toString());
    }

    public static Grupo encontrarGrupoMasGrande(ArrayList<Grupo> listaGrupos) {
        if (listaGrupos.isEmpty()) {
            return null;
        }

        Grupo grupoMasGrande = listaGrupos.get(0);

        for (Grupo grupo : listaGrupos) {
            if (grupo.tamanioGrupo > grupoMasGrande.tamanioGrupo) {
                grupoMasGrande = grupo;
            }
        }

        return grupoMasGrande;
    }

    public static int eliminarGrupo(ArrayList<Grupo> listaGrupos, Grupo grupoAEliminar, char[][] matriz) {
        // imprimirGrupos(listaGrupos);

        // devuelve -1 si el grupo a eliminar tiene solo una ficha (es por si acaso)
        if (grupoAEliminar.tamanioGrupo <= 1) {
            return -1;
        }

        listaGrupos.remove(grupoAEliminar);

        int[] posicionAbajoIzquierda = encontrarPosicionMasAbajoIzquierda(grupoAEliminar);
        posicionAbajoIzquierda = convertirPosicionSalida(posicionAbajoIzquierda, matriz);

        for (int[] posiciones : grupoAEliminar.posiciones) {
            int fila = posiciones[0];
            int columna = posiciones[1];
            matriz[fila][columna] = ' ';
        }

        return grupoAEliminar.tamanioGrupo;
    }

    public static int[] convertirPosicionSalida(int[] posicion, char[][] matriz) {
        int[] aux = new int[2];

        // A las filas, cogemos el num de filas de la matriz y le restamos la posicion
        // original
        aux[0] = matriz.length - posicion[0];

        // A las columnas le suma 1
        aux[1] = posicion[1] + 1;

        return aux;
    }

    public static int[] encontrarPosicionMasAbajoIzquierda(Grupo grupo) {
        int[] posicion = { Integer.MIN_VALUE, Integer.MAX_VALUE };

        for (int[] aux : grupo.posiciones) {
            posicion[0] = Math.max(posicion[0], aux[0]);
        }

        for (int[] aux : grupo.posiciones) {
            if (posicion[0] == aux[0]) {
                posicion[1] = Math.min(posicion[1], aux[1]);
            }
        }

        return posicion;
    }

    public static char[][] recrearMatriz(char[][] matriz) {
        // System.out.println("entrado en recrearMatriz");
        moverAbajo(matriz);
        // System.out.println("movido abajo");

        // System.out.println(encontrarColumnaVacia(matriz));
        matriz = moverIzquierda(matriz);
        // System.out.println("movido izq");

        return matriz;
    }

    public static char[][] redimensionarMatriz(char[][] matrizOriginal, int columnaAEliminar) {
        if (columnaAEliminar < 0 || columnaAEliminar >= matrizOriginal[0].length) {
            return matrizOriginal; // Devuelve la matriz original si la columna no es válida
        }

        int filas = matrizOriginal.length;
        int columnas = matrizOriginal[0].length - 1; // Reduce la cantidad de columnas

        char[][] matrizRedimensionada = new char[filas][columnas];

        for (int i = 0; i < filas; i++) {
            int destCol = 0;
            for (int j = 0; j < matrizOriginal[i].length; j++) {
                if (j != columnaAEliminar) {
                    matrizRedimensionada[i][destCol++] = matrizOriginal[i][j];
                }
            }
        }

        return matrizRedimensionada;
    }

    public static void moverAbajo(char[][] matriz) {
        int filas = matriz.length;
        int columnas = matriz[0].length;

        // Paso 1: Mover hacia abajo
        for (int columna = 0; columna < columnas; columna++) {
            int filaDestino = filas - 1;
            for (int fila = filas - 1; fila >= 0; fila--) {
                if (matriz[fila][columna] != ' ') {
                    matriz[filaDestino][columna] = matriz[fila][columna];
                    if (filaDestino != fila) {
                        matriz[fila][columna] = ' ';
                    }
                    filaDestino--;
                }
            }
        }
    }

    public static char[][] moverIzquierda(char[][] matriz) {
        
        while (encontrarColumnaVacia(matriz) != -1) {
            matriz = eliminarColumnaVacia(matriz, encontrarColumnaVacia(matriz));
            
            matriz = redimensionarMatriz(matriz, matriz[0].length - 1);
        }

        return matriz;
    }

    public static char[][] eliminarColumnaVacia(char[][] matriz, int indice) {
        // mueve todo lo que esta a la derecha de la columna vacia a la izquierda
        for (int columna = indice; columna < matriz[0].length - 1; columna++) {
            for (int fila = 0; fila < matriz.length; fila++) {
                matriz[fila][columna] = matriz[fila][columna + 1];
            }
        }

        // convierte la ultima fila en espacios en blanco
        for (int fila = 0; fila < matriz.length; fila++) {
            matriz[fila][matriz[0].length - 1] = ' ';
        }

        return matriz;
        
    }

    // devuelve el indice de la primera columna vacia que encuentra
    public static int encontrarColumnaVacia(char[][] matriz) {
        int filas = matriz.length;
        int columnas = matriz[0].length;

        for (int columna = 0; columna < columnas; columna++) {
            boolean columnaVacia = true;

            for (int fila = 0; fila < filas; fila++) {
                if (matriz[fila][columna] != ' ') {
                    columnaVacia = false;
                    break;
                }
            }

            // si se ha encontrado columna vacia y esta no esta a la derecha del todo
            if (columnaVacia && columna < matriz[0].length - 1) {
                return columna;
            }
        }

        return -1; // No se encontraron columnas vacías
    }

    public static String pluralizarString(String linea, int valor) {
        if (valor == 1) {
            return linea;
        } else {
            return linea + "s";
        }
    }

    public static char[][] copiarMatriz(char[][] matriz) {
        char[][] nuevaMatriz = new char[matriz.length][matriz[0].length];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                nuevaMatriz[i][j] = matriz[i][j];
            }
        }
        return nuevaMatriz;
    }

    public static boolean listaContieneGrupo(ArrayList<Grupo> lista, Grupo grupo) {
        for (Grupo aux : lista) {
            if (aux.letra == grupo.letra) {
                if (aux.tamanioGrupo == grupo.tamanioGrupo) {
                    if (mismasPosiciones(aux.posiciones, grupo.posiciones)) {
                        if (aux.equals(grupo)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private static boolean mismasPosiciones(ArrayList<int[]> posiciones, ArrayList<int[]> posiciones2) {
        boolean mismas = true;

        for (int i = 0; i < posiciones.size(); i++) {
            if (posiciones.get(i)[0] != posiciones.get(i)[0] || posiciones.get(i)[1] != posiciones.get(i)[1]) {
                mismas = false;
            }
        }
        return mismas;
    }

    public static ArrayList<Movimiento> copiarMovimientos(ArrayList<Movimiento> movimientos) {
        ArrayList<Movimiento> nuevosMovimientos = new ArrayList<>();

        for (Movimiento movimiento : movimientos) {
            nuevosMovimientos.add(movimiento);
        }
        return nuevosMovimientos;
    }
}